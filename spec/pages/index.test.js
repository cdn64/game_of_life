import {shallow} from "enzyme"
import React from "react"
import renderer from "react-test-renderer"

import Index from "../../pages/index.js"
const IndexPage = shallow(<Index />)

describe("index page", () => {
  it("renders a Game", () => {
    expect(IndexPage.find("Game").length).toEqual(1)
  })
})
