import {mount} from "enzyme"
import React from "react"

import Game from "../../container/Game.js"

const initialState = [[true, true], [true, false]]
const game = mount(<Game width={2} height={2} initialState={initialState} />)
const field = game.find("Field").at(0)
const cells = game.find("Cell")

it("renders a Field", () => {
  expect(game.find("Field").length).toEqual(1)
})

it("sends initialState down to cells", () => {
  expect(cells.at(0).props().isAlive).toEqual(true)
})

it("renders an empty field when no initialState given", () => {
  const emptyGame = mount(<Game width={2} height={2} />)
  const firstCell = emptyGame.find("Cell").at(0)
  expect(firstCell.props().isAlive).toEqual(false)
})

it("toggles Cells' isAlive when clicked", () => {
  const wasAlive = field.props().areCellsAlive[0][0]
  cells.at(0).simulate("click")
  game.update()
  expect(field.props().areCellsAlive[0][0]).toEqual(!wasAlive)
})

it("renders game controls", () => {
  expect(game.find("GameControls").length).toEqual(1)
})

it("holds isPlaying state and will step forward automatically", () => {
  jest.useFakeTimers()

  const oneCellGame = mount(
    <Game width={1} height={1} initialState={[[true]]} />
  )
  const playPauseButton = oneCellGame
    .find("GameControls")
    .find("button")
    .at(1)
  expect(playPauseButton.text()).toEqual("Play")
  playPauseButton.simulate("click")
  expect(playPauseButton.text()).toEqual("Pause")

  jest.runOnlyPendingTimers()
  oneCellGame.update()

  expect(
    oneCellGame
      .find("Cell")
      .first()
      .props().isAlive
  ).toEqual(false)
})

describe("game logic", () => {
  const onChange = jest.fn()
  const firstState = [
    [false, true, false],
    [true, true, false],
    [true, true, false]
  ]
  const secondState = [
    [true, true, false],
    [false, false, true],
    [true, true, false]
  ]
  const thirdState = [
    [false, true, false],
    [false, false, true],
    [false, true, false]
  ]
  const game = mount(
    <Game width={3} height={3} initialState={firstState} onChange={onChange} />
  )
  const stepForward = () =>
    game
      .find("GameControls")
      .find("button")
      .at(0)
      .simulate("click")

  it("steps forward when asked to and correctly calculates second step", () => {
    stepForward()
    expect(onChange.mock.calls.length).toEqual(1)
    expect(onChange.mock.calls[0][0]).toEqual(secondState)
  })

  it("correctly calculates third step", () => {
    stepForward()
    expect(onChange.mock.calls[1][0]).toEqual(thirdState)
  })
})
