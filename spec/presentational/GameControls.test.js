import {shallow} from "enzyme"
import React from "react"

import GameControls from "../../presentational/GameControls.js"

const stepForward = jest.fn()
const togglePlaying = jest.fn()
const gameControls = shallow(
  <GameControls onStepForward={stepForward} onTogglePlaying={togglePlaying} />
)
const buttons = gameControls.find("button")

it("renders two buttons", () => {
  expect(buttons.length).toEqual(2)
})

it("renders a clickable button to step forward", () => {
  buttons.at(0).simulate("click")
  expect(stepForward.mock.calls.length).toEqual(1)
})

it("renders a clickable button to play / pause", () => {
  buttons.at(1).simulate("click")
  expect(togglePlaying.mock.calls.length).toEqual(1)
})

it("shows isPlaying state on play / pause button", () => {
  const playingGameControls = shallow(<GameControls isPlaying={true} />)
  const pausingGameControls = shallow(<GameControls isPlaying={false} />)
  expect(
    playingGameControls
      .find("button")
      .at(1)
      .text()
  ).toEqual("Pause")
  expect(
    pausingGameControls
      .find("button")
      .at(1)
      .text()
  ).toEqual("Play")
})
