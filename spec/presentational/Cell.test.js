import {shallow} from "enzyme"
import React from "react"

import Cell from "../../presentational/Cell.js"

describe("Cell", () => {
  it("renders a single <rect> only if provided prop isAlive is true", () => {
    const livingCell = shallow(<Cell isAlive={true} />)
    const deadCell = shallow(<Cell isAlive={false} />)

    expect(livingCell.find("rect").props().fill).toEqual("#000")
    expect(deadCell.find("rect").props().fill).toEqual("#FFF")
  })

  it("renders a <rect> with a defined size", () => {
    const cell = shallow(<Cell isAlive={true} />)
    const props = cell.find("rect").props()

    expect(props).toHaveProperty("width")
    expect(props).toHaveProperty("height")
  })
})
