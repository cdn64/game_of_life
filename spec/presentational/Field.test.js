import {mount} from "enzyme"
import React from "react"

import Field from "../../presentational/Field.js"

import {CELL_SIZE} from "../../lib/constants"

const field = mount(
  <Field width={2} height={2} areCellsAlive={[[true, false], [false, true]]} />
)
const gs = field.find("g")
const cells = field.find("Cell")

describe("Field", () => {
  it("renders a svg element", () => {
    expect(field.find("svg").length).toEqual(1)
  })

  it("relays alive state to Cell", () => {
    expect(cells.at(0).props().isAlive).toEqual(true)
    expect(cells.at(1).props().isAlive).toEqual(false)
  })

  it("renders a square of Cells", () => {
    expect(cells.length).toEqual(4)
  })

  it("renders Cells within a <g> with translation", () => {
    expect(field.find("g").length).toEqual(4)
    expect(gs.first().props().transform).toEqual("translate(0,0)")
    expect(gs.last().props().transform).toEqual(
      `translate(${CELL_SIZE},${CELL_SIZE})`
    )
  })
})
