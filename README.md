# Game of Life

This is a very simple implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).

## Available tasks

* `yarn dev` will start the development server
* `yarn test` will start the jest test watcher
