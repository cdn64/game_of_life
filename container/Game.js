import React from "react"

import Field from "../presentational/Field"
import GameControls from "../presentational/GameControls"

export default class Game extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      areCellsAlive: this.props.initialState || this.getEmptyState(),
      isPlaying: false
    }
  }

  getEmptyState() {
    const xAxis = Array.from(Array(this.props.width).keys())
    const yAxis = Array.from(Array(this.props.height).keys())

    return xAxis.map(x => yAxis.map(y => false))
  }

  onCellClick(x, y) {
    this.setState(state => {
      let areCellsAlive = state.areCellsAlive
      areCellsAlive[x][y] = !areCellsAlive[x][y]
      return {areCellsAlive}
    })
  }

  calculateNeighbourCount(x, y) {
    const neighbours = [
      [-1, -1],
      [0, -1],
      [1, -1],
      [-1, 0],
      [1, 0],
      [-1, 1],
      [0, 1],
      [1, 1]
    ]
    const {width, height} = this.props
    const count = neighbours
      .map(([diffX, diffY]) => [x + diffX, y + diffY])
      .filter(([x, y]) => x >= 0 && x < width && y >= 0 && y < height)
      .map(([x, y]) => this.state.areCellsAlive[x][y])
      .reduce((sum, isAlive) => sum + (isAlive ? 1 : 0), 0)
    return count
  }

  calculateAlive(previousState, x, y) {
    const neighbourCount = this.calculateNeighbourCount(x, y)
    if (neighbourCount === 3) {
      return true
    } else if (neighbourCount > 3 || neighbourCount < 2) {
      return false
    } else {
      return previousState
    }
  }

  stepForward() {
    let areCellsAlive = this.state.areCellsAlive
    areCellsAlive = areCellsAlive.map((row, x) =>
      row.map((cell, y) => this.calculateAlive(cell, x, y))
    )
    this.setState({areCellsAlive}, () => {
      this.props.onChange && this.props.onChange(this.state.areCellsAlive)
    })
  }

  play() {
    this.timeout = setInterval(() => this.stepForward(), 500)
  }
  stopPlaying() {
    clearTimeout(this.timeout)
  }

  togglePlaying() {
    this.setState(state => {
      if (!state.isPlaying) {
        this.play()
      } else {
        this.stopPlaying()
      }
      return {isPlaying: !state.isPlaying}
    })
  }

  render() {
    return (
      <React.Fragment>
        <GameControls
          isPlaying={this.state.isPlaying}
          onStepForward={this.stepForward.bind(this)}
          onTogglePlaying={this.togglePlaying.bind(this)}
        />
        <Field
          width={this.props.width}
          height={this.props.height}
          areCellsAlive={this.state.areCellsAlive}
          onCellClick={this.onCellClick.bind(this)}
        />
      </React.Fragment>
    )
  }
}
