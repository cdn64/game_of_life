import React from "react"

export default class GameControls extends React.Component {
  render() {
    return (
      <React.Fragment>
        <button onClick={this.props.onStepForward}>Step forward</button>
        <button onClick={this.props.onTogglePlaying}>
          {this.props.isPlaying ? "Pause" : "Play"}
        </button>
      </React.Fragment>
    )
  }
}
