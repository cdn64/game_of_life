import React from "react"

import {CELL_SIZE} from "../lib/constants"

export default class Cell extends React.Component {
  onClick() {
    this.props.onClick && this.props.onClick()
  }
  render() {
    return (
      <rect
        width={CELL_SIZE - 2}
        height={CELL_SIZE - 2}
        onClick={this.onClick.bind(this)}
        fill={this.props.isAlive ? "#000" : "#FFF"}
        stroke="#EEE"
      />
    )
  }
}
