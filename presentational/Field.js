import React from "react"

import Cell from "./Cell"
import {CELL_SIZE} from "../lib/constants"

export default class Field extends React.Component {
  onCellClick(x, y) {
    this.props.onCellClick && this.props.onCellClick(x, y)
  }

  render() {
    const xAxis = Array.from(Array(this.props.width).keys())
    const yAxis = Array.from(Array(this.props.height).keys())
    let index = 0

    return (
      <svg
        width={this.props.width * CELL_SIZE}
        height={this.props.height * CELL_SIZE}
      >
        {xAxis.map(x =>
          yAxis.map(y => {
            index = index + 1
            return (
              <g
                key={index}
                transform={`translate(${CELL_SIZE * x},${CELL_SIZE * y})`}
              >
                <Cell
                  isAlive={this.props.areCellsAlive[x][y]}
                  onClick={() => this.onCellClick(x, y)}
                />
              </g>
            )
          })
        )}
      </svg>
    )
  }
}
